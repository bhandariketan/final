﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm.BusinessLogic
{
    class JobOpening
    {
        public int ID { get; } //change to readonly property
        public string companyName { get; private set; }  //change to public read - private write property
        public string Title { get; set; }  //change to read-write property
        public int Experience { get; set; }//change to read-write property

        //JobType as enum
        EducationLevel EdLevel; 

        //Instance of PayBand
        PayBand payBand;

        //Constructor
        public JobOpening( int id, string companyname, string title, int experience, int minimumPay, int maximumPay, int yearlyPay )
        {
            ID = id;
            companyName = companyname;
            Title = title;
            Experience = experience;
            EdLevel = new EducationLevel();
            payBand = new PayBand( minimumPay, maximumPay, yearlyPay);



        }
    }

}
