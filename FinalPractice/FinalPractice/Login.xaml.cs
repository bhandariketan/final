﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace FinalPractice
{
    public enum LoginResult
    {
        SUCCESS,
        FAILURE,
        CANCELLED
    }
    public sealed partial class Login : ContentDialog
    {
        public LoginResult LoginResult { get; set; }
        public Login()
        {
            this.InitializeComponent();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            ApplicationDataContainer settings = ApplicationData.Current.LocalSettings;
            string username = "";

            string password = "";

            if (settings.Values["Username"] != null && settings.Values["Password"] != null)
            {
                username = settings.Values["Username"].ToString();
                password = settings.Values["Password"].ToString();
            }
            else
            {
                username = password = "test";
            }




            if (!string.IsNullOrEmpty(TxtUsername.Text)
                &&
                !string.IsNullOrEmpty(TxtPassword.Password)
                &&
                TxtUsername.Text.Equals(username) && TxtPassword.Password.Equals(password)
                )
            {
                LoginResult = LoginResult.SUCCESS;
                
            }
            else
                LoginResult = LoginResult.FAILURE;
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}
