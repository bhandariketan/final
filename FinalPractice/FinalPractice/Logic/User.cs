﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalPractice.Logic
{
    class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Profile { get; set; }
        public int Id { get; set; }
        public string Email { get; set; }
        public User(string username, string password, string profile, int id, string email)
        {
            Username = username;
            Password = password;
            Profile = profile;
            Id = id;
            Email = email;
        }
    }
}
