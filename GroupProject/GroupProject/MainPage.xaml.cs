﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Media.Playback;
using Windows.Media.Core;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        MediaPlayer music;
        MediaPlayer click;
        bool musicplay;
        public MainPage()
        {
            this.InitializeComponent();
            music = new MediaPlayer();
            click = new MediaPlayer();
            musicplay = false;
            NavigationCacheMode = NavigationCacheMode.Enabled;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {

            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            StorageFile file = await folder.GetFileAsync("sound.mp3");
            music.AutoPlay = true;
            music.Source = MediaSource.CreateFromStorageFile(file);
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (musicplay)
            {
                music.Pause();
                musicplay = false;
                imgSound.Source = new BitmapImage(new Uri("ms-appx:///Assets/off.png"));
                
            }
            else
            {
                music.Play();
                musicplay = true;
                imgSound.Source = new BitmapImage(new Uri("ms-appx:///Assets/on.png"));

            }
        }

        private async void MainMenu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            StorageFile file = await folder.GetFileAsync("Button.mp3");
            click.AutoPlay = true;
            click.Source = MediaSource.CreateFromStorageFile(file);

            if (sender.Equals(txtPlay) || sender.Equals(imgPlay))
            {
                this.Frame.Navigate(typeof(PlayerInfo));
                music.Source = null;
                musicplay = false;
            }
            else if (sender.Equals(txtResults) || sender.Equals(imgResults))
            {
                this.Frame.Navigate(typeof(Results));
                music.Source = null;
                musicplay = false;
            }
            else if (sender.Equals(txtExit) || sender.Equals(imgExit))
            {
                Application.Current.Exit();

            }

            

                
        }

        
    }
}
