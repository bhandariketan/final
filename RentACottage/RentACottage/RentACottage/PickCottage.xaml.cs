﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using RentACottage.BusinessLogic;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace RentACottage
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PickCottage : Page
    {
        private List<Cottage> Cottages { get; set; }
        DataContractJsonHandler handler = new DataContractJsonHandler(ApplicationData.Current.RoamingFolder.Path + "\\" + "Cottage.json");
        public PickCottage()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Cottages = handler.Load();
            LVCottages.ItemsSource = Cottages;
        }

        private void BtnBook_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RentCottage));
        }
    }
}
