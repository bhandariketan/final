﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using RentACottage.BusinessLogic;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace RentACottage
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RentCottage : Page
    {
        private List<BookingRequest> booking { get; set; }
        DataContractHandler handler = new DataContractHandler(ApplicationData.Current.RoamingFolder.Path + "\\" + "booking.xml");
        public RentCottage()
        {
            this.InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            booking = handler.Load();
            

        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string BookingName = TxtBookedBy.Text;
            string NoofDays = TxtNumberOfDays.Text;
            string dob = DPVisitDate.Date.ToString();

            if( BookingName.Length == 0 || NoofDays.Length == 0 || dob.Length == 0)
            {
                error();
            }

            booking = new List<BookingRequest>(BookingName, NoofDays, dob);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            handler.Store(booking);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(PickCottage));
        }

        public async void error()
        {
            ContentDialog Error = new ContentDialog()
            {
                Title = "Error",
                Content = "Invalid Entry",
                CloseButtonText = "OK"

            };
            await Error.ShowAsync();


        }
    }
}
