﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace RentACottage.BusinessLogic
{
    class DataContractJsonHandler
    {
        string FileName;
        public DataContractJsonHandler(string fileName)
        {
            FileName = fileName;
        }
        public void Store(List<Cottage> cottages)
        {
            DataContractJsonSerializer dcSerializer = new DataContractJsonSerializer(typeof(List<Cottage>));
            using (FileStream fileStream = new FileStream(FileName, FileMode.Create))
            {
                dcSerializer.WriteObject(fileStream, cottages);
            }
        }

        public List<Cottage> Load()
        {
            List<Cottage> cottages = null;
            DataContractJsonSerializer xmlSerializer = new DataContractJsonSerializer(typeof(List<Cottage>));
            using (FileStream fileStream = new FileStream(FileName, FileMode.Open))
            {
                cottages = (List<Cottage>)xmlSerializer.ReadObject(fileStream);
            }
            return cottages;
        }
    }
}
