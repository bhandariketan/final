﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACottage.BusinessLogic
{
    class BookingRequest
    {
        public int BookingNo { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
        public DateTime VisitDate { get; set; }
        public int NoOfDays { get; set; }

        public BookingRequest(int bookingNo, string requestedBy, DateTime requestedOn, DateTime visitDate, int noOfDays)
        {
            BookingNo = bookingNo;
            RequestedBy = requestedBy;
            RequestedOn = requestedOn;
            VisitDate = visitDate;
            NoOfDays = noOfDays;
        } 




        
    }
}
