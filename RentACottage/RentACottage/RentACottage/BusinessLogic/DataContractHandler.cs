﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RentACottage.BusinessLogic
{
    class DataContractHandler
    {
        string FileName;
        public DataContractHandler(string fileName)
        {
            FileName = fileName;
        }
        public void Store(List<BookingRequest> bookings)
        {
            DataContractSerializer dcSerializer = new DataContractSerializer(typeof(List<BookingRequest>));
            using (FileStream fileStream = new FileStream(FileName, FileMode.Create))
            {
                dcSerializer.WriteObject(fileStream, bookings);
            }
        }

        public List<BookingRequest> Load()
        {
            List<BookingRequest> bookings = null;
            DataContractSerializer xmlSerializer = new DataContractSerializer(typeof(List<BookingRequest>));
            using (FileStream fileStream = new FileStream(FileName, FileMode.Open))
            {
                bookings = (List<BookingRequest>)xmlSerializer.ReadObject(fileStream);
            }
            return bookings;
        }
    }
}
