﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACottage.BusinessLogic
{
    public class Cottage
    {
        public int CottageID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int Bedrooms { get; set; }
        public int PricePerNight { get; set; }
        public string ImagePath { get; set; }
        public string[] Features { get; set; }

        //Add Constructor
        public Cottage(int cottageid, string name, string location, int bedroom, int pricepernight, string imagePath, string[] features)
        {
            CottageID = cottageid;
            Name = name;
            Location = location;
            Bedrooms = bedroom;
            PricePerNight = pricepernight;
            ImagePath = imagePath;
            Features = features;

        }

        //Override ToString as per format in Question Paper
        public override string ToString()
        {
            return $"{CottageID} near {Location} with {Bedrooms}, @{PricePerNight}/Night.";

        }

    }
}
