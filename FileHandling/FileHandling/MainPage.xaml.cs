﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FileHandling
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void BSave_Click(object sender, RoutedEventArgs e)
        {
            // StorageFile storageFile = await StorageFile.GetFileFromPathAsync(@"e:\test.txt");
            // will show exceptional erroe as we don't have the access of the file that is out of the application

            //StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("test.txt");
            //await FileIO.WriteTextAsync(file, txtContent.Text);

            FileSavePicker savePicker = new FileSavePicker();
            List<string> fileExtensions = new List<string>();
            fileExtensions.Add(".txt");
            fileExtensions.Add(".text");
            savePicker.FileTypeChoices.Add("Text File", fileExtensions);
            StorageFile file = await savePicker.PickSaveFileAsync();
            await FileIO.WriteTextAsync(file, txtContent.Text);

            
        }

        private async void BLoad_Click(object sender, RoutedEventArgs e)
        {
            //string filePath = ApplicationData.Current.LocalFolder.Path + @"\test.txt";
            //StorageFile file = await StorageFile.GetFileFromPathAsync("test.txt");
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.FileTypeFilter.Add(".txt");
            StorageFile file = await openPicker.PickSingleFileAsync();
          

            txtContent.Text = await FileIO.ReadTextAsync(file);
        }
    }
}
