﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupProject.BusinessLogic
{
    public class Tile
    {
        private TileState tileState;
        
        public Image ImageControl { get; set; }

        public Uri ImageUri { get; set; }

        public Tile(Image imageControl)
        {
            ImageControl = imageControl;
            tileState = TileState.Blank;
        }
        
        public TileState TileState
        {
            get { return tileState; }
            set
            {
                tileState = value;
                if (value == TileState.PlayerOne)
                {
                    ImageUri = new Uri("ms-appx:///Images/x.png");
                    ImageControl.Source = new BitmapImage(ImageUri);
                }
                    
                else if (value == TileState.PlayerTwo)
                {
                    ImageUri = new Uri("ms-appx:///Images/o.png");
                    ImageControl.Source = new BitmapImage(ImageUri);
                }

                else if (value == TileState.Blank)
                {
                    ImageUri = new Uri("ms-appx:///Assets/empty.png");
                    ImageControl.Source = new BitmapImage(ImageUri);
                }
                   
            }
        }
    }
}
