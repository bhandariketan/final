﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.Media.Playback;
using Windows.Media.Core;
using Windows.Storage;

namespace GroupProject.BusinessLogic
{
    public class Game
    {
        public int GameID { get; set; }
        public GameBoard GBoard { get; set; }
        public Player PlayerOne { get; set; }
        public Player PlayerTwo { get; set; }
        public int POneWins { get; set; }
        public int PTwoWins { get; set; }
        MediaPlayer sound = new MediaPlayer();

        
        public TextBlock txtTurn;
        public TextBlock txtSession;

        public Game(Player pOne, Player pTwo, Image[] images, TextBlock txtTurn, TextBlock txtSession)
        {
            PlayerOne = pOne;
            PlayerTwo = pTwo;
            this.txtTurn = txtTurn;
            this.txtSession = txtSession;
            POneWins = 0;
            PTwoWins = 0;
            GBoard = new GameBoard(images);        
        }

        public void newGame()
        {
            GBoard.ResetGameboard();
            UpdateSession();
            txtTurn.Text = $"{PlayerOne.PlayerName}'s Turn";
        }

        public async void playerWonDialog(Player player)
        {
           
            string text = ($"{player.PlayerName} has won");
            var dialog = new MessageDialog(text);
            await dialog.ShowAsync();
            txtTurn.Text = $"{PlayerTwo.PlayerName}'s Turn";
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            StorageFile file = await folder.GetFileAsync("Click.mp3");
            sound.AutoPlay = true;
            sound.Source = MediaSource.CreateFromStorageFile(file);


        }

        public async void NoWinnerDialog()
        {
            var dialog = new MessageDialog("Tie");
            await dialog.ShowAsync();
        }

        public async void OnImageTapped(Image imgSender)
        {
            
            GBoard.SetTile(imgSender);
            TileState check = GBoard.CheckForWin();
            if (check == TileState.PlayerOne)
            {
                playerWonDialog(PlayerOne);
                POneWins += 1;
                newGame();
            }
            else if (check == TileState.PlayerTwo)
            {
                playerWonDialog(PlayerTwo);
                PTwoWins += 1;
                newGame();
            }
            else if (GBoard.Round == 10)
            {
                NoWinnerDialog();
                newGame();
            }

            if (GBoard.Round%2 == 0)
            {
                txtTurn.Text = $"{PlayerTwo.PlayerName}'s Turn";
                StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
                StorageFile file = await folder.GetFileAsync("Switch.mp3");
                sound.AutoPlay = true;
                sound.Source = MediaSource.CreateFromStorageFile(file);


            }
            else
            {
                txtTurn.Text = $"{PlayerOne.PlayerName}'s Turn";
                StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
                StorageFile file = await folder.GetFileAsync("Tick.mp3");
                sound.AutoPlay = true;
                sound.Source = MediaSource.CreateFromStorageFile(file);
            }
        }

        public void UpdateSession()
        {
            txtSession.Text = $"{PlayerOne.PlayerName}: {POneWins} - {PlayerTwo.PlayerName}: {PTwoWins}";
        }
    }
}
