﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject.BusinessLogic
{
    public enum TileState
    {
        Blank,
        PlayerOne,
        PlayerTwo
    }
}
