﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject.BusinessLogic
{
    public class Player
    {
        public string PlayerName { get; set; }
        public int TotalWins { get; set; }

        public Player(string name)
        {
            PlayerName = name;
        }
    }
}
