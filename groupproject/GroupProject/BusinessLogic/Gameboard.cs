﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupProject.BusinessLogic
{
    public class GameBoard
    {
        public Tile[] ListOfTiles;
        public Image[] Images;
        public int Round { get; set; }


        public GameBoard(Image[] images)
        {
            Round = 1;
            Images = images;
            ListOfTiles = GenerateTileList();
        }

        public Tile[] GenerateTileList()
        {
            ListOfTiles = new Tile[9];

            for (int listLength = 0; listLength < 9; listLength++)
            {
                ListOfTiles[listLength] = new Tile(Images[listLength]);
            }
            return ListOfTiles;
        }

        public void ResetGameboard()
        {
            for (int listLength = 0; listLength < 9; listLength++)
            {
                ListOfTiles[listLength].TileState = TileState.Blank;
            }
            Round = 1;
        }

        public void SetTile(Image image)
        {
            for (int i = 0; i < ListOfTiles.Length; i++)
            {
                if (image.Name == ListOfTiles[i].ImageControl.Name)
                {
                    if (ListOfTiles[i].TileState == TileState.Blank)
                    {
                        if (Round % 2 == 0)
                        {
                            ListOfTiles[i].TileState = TileState.PlayerTwo;
                        }
                        else
                        {
                            ListOfTiles[i].TileState = TileState.PlayerOne;
                        }
                        this.Round += 1;
                        break;
                    }

                }
            }
        }

        public TileState CheckForWin()
        {
            TileState checkWinner = TileState.Blank;

            checkWinner = CheckHorizontal();

            if (checkWinner == 0)
                checkWinner = CheckVertical();

            if (checkWinner == 0)
                checkWinner = CheckDiagonal();


            return checkWinner;

        }
      
        public TileState CheckHorizontal()
        {
            TileState playerTile;
            TileState winnerTile = TileState.Blank;

            for (int rowCheck = 0; rowCheck < 9; rowCheck += 3)
            {
                playerTile = ListOfTiles[rowCheck].TileState;
                
                if (playerTile == TileState.PlayerOne || playerTile == TileState.PlayerTwo)
                {
                    if (ListOfTiles[rowCheck + 1].TileState == playerTile)
                    {
                        if (ListOfTiles[rowCheck + 2].TileState == playerTile)
                            winnerTile = playerTile;
                    }
                }              
            }
            return winnerTile;
        }

        public TileState CheckVertical()
        {
            TileState playerTile;
            TileState winnerTile = TileState.Blank;

            for (int rowCheck = 0; rowCheck < 3; rowCheck += 1)
            {
                playerTile = ListOfTiles[rowCheck].TileState;

                if (playerTile == TileState.PlayerOne || playerTile == TileState.PlayerTwo)
                {
                    if (ListOfTiles[rowCheck + 3].TileState == playerTile)
                    {
                        if (ListOfTiles[rowCheck + 6].TileState == playerTile)
                            winnerTile = playerTile;
                    }
                }       
            }
            return winnerTile;

        }

        public TileState CheckDiagonal()
        {
            TileState playerTile;
            TileState winnerTile = TileState.Blank;

            playerTile = ListOfTiles[0].TileState;

            if (playerTile == TileState.PlayerOne || playerTile == TileState.PlayerTwo)
            {
                if (ListOfTiles[4].TileState == playerTile)
                {
                    if (ListOfTiles[8].TileState == playerTile)
                        winnerTile = playerTile;
                }
            }

            if (winnerTile != TileState.Blank)
                return winnerTile;

            playerTile = ListOfTiles[2].TileState;

            if (playerTile == TileState.PlayerOne || playerTile == TileState.PlayerTwo)
            {
                if (ListOfTiles[4].TileState == playerTile)
                {
                    if (ListOfTiles[6].TileState == playerTile)
                        winnerTile = playerTile;
                }
            }      
            return winnerTile;
        }

    }
}
