﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Media.Core;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlayerInfo : Page
    {
        public string pOne { get; set; }
        public string pTwo { get; set; }
        MediaPlayer playButton;

        public PlayerInfo()
        {
            this.InitializeComponent();
            playButton = new MediaPlayer();
        }

        private async void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPOne.Text) && !string.IsNullOrEmpty(txtPTwo.Text))
            {
                pOne = txtPOne.Text;
                pTwo = txtPTwo.Text;

                ApplicationDataContainer storage = ApplicationData.Current.LocalSettings;

                storage.Values["Player1"] = pOne;
                storage.Values["Player2"] = pTwo;
                this.Frame.Navigate(typeof(GamePage));
            }
            else
            {
                error();
            }

            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            StorageFile file = await folder.GetFileAsync("Click.mp3");
            playButton.AutoPlay = true;
            playButton.Source = MediaSource.CreateFromStorageFile(file);
        }

        public async void error()
        {
            ContentDialog Error = new ContentDialog()
            {
                Title = "Error",
                Content = "Please input your names",
                CloseButtonText = "OK"
            };

            await Error.ShowAsync();
        }


        

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var currentView = SystemNavigationManager.GetForCurrentView();
            currentView.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            currentView.BackRequested += backButton_Tapped;
        }

        private void backButton_Tapped(object sender, BackRequestedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
            var currentView = SystemNavigationManager.GetForCurrentView();
            if (Frame.Navigate(typeof(MainPage)) == true)
            {
                currentView.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Disabled;
            }
        }
    }
}
