﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using GroupProject.BusinessLogic;
using Windows.Storage;
using Windows.Media.Playback;
using Windows.Media.Core;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        Game game;
        Image[] images;
        int Call;
        

        public GamePage()
        {
            this.InitializeComponent();
            Call = 1;
            
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(btnPlyAgn))
            {
                game.newGame();
                Call = 1;
            }
            

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string pone = "";
            string ptwo = "";
            ApplicationDataContainer storage = ApplicationData.Current.LocalSettings;
            if (storage.Values["Player1"] != null && storage.Values["Player2"] != null)
            {
                pone = storage.Values["Player1"].ToString();

                ptwo = storage.Values["Player2"].ToString();
                
            }
            Player p1 = new Player(pone);
            Player p2 = new Player(ptwo);

            //txtTurn.Text = $"{p1.PlayerName}'s turn";

            images = new Image[] { ImgOne, ImgTwo, ImgThree, ImgFour, ImgFive, ImgSix, ImgSeven, ImgEight, ImgNine };
            game = new Game(p1, p2, images, txtTurn, txtSession);
            game.newGame();
            foreach (Image image in images)
                image.Tapped += OnImageTapped;
        }

        //this method gets called twice on image tap for unknown reasons
        private  void OnImageTapped(object sender, TappedRoutedEventArgs e)
        {
            if (Call%2 == 0)
            {
                

            }
            else
            {
                game.OnImageTapped((Image)sender);
            }
            Call += 1;

        }
    }
}
