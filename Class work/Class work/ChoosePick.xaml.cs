﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Class_work
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ChoosePick : Page
    {
        public ChoosePick()
        {
            this.InitializeComponent();
        }

        private async void BPickImage_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".png");
            openPicker.FileTypeFilter.Add(".bmp");
            openPicker.FileTypeFilter.Add(".gif");
            StorageFile imgFile = await openPicker.PickSingleFileAsync();

            StorageFolder imgFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("Images", CreationCollisionOption.OpenIfExists);

            if (imgFile != null && imgFolder!= null){
                await imgFile.CopyAsync(imgFolder, imgFile.Name, NameCollisionOption.ReplaceExisting);
            }

            //can create another method or maybe on page load you can 
            //1. read the profile image from the folder Images in LocalFolder
            //2. Do the following to display the image in the Image Control
            //var stream = await imgFile.OpenReadAsync();
            //BitmapImage memImage = new BitmapImage();
            //memImage.SetSource(stream);
            //ImgProfilePic.Source = memImage;
            //stream.Dispose();

            //similar to try=finally the using block would auto-dispose the resources on exit
            using (var stream = await imgFile.OpenReadAsync())
            {
                BitmapImage memImage = new BitmapImage();
                memImage.SetSource(stream);
                ImgProfilePic.Source = memImage;
                stream.Dispose();

            }

        }
    }
}
